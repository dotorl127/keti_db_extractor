#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <pcl_conversions/pcl_conversions.h>
#include "pcl_ros/transforms.h"

#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/point_field_conversion.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <novatel_gps_msgs/Inspva.h>
#include <novatel_oem7_msgs/INSPVA.h>
#include <can_msgs/Frame.h>
#include <als20_ambient_light_sensor/Illuminance.h>
#include <control_msgs/ChassisRadarData.h>

#include <iostream>
#include <keti_db_extractor/keti_db_extractor.h>

#include <ouster_ros/os_ros.h>
#include <chrono>

namespace ouster_sensor = ouster::sensor;
using namespace std::chrono_literals;
namespace fsys = boost::filesystem;

typedef sensor_msgs::PointCloud2 PointCloud;
typedef PointCloud::Ptr PointCloudPtr;
typedef PointCloud::ConstPtr PointCloudConstPtr;

int main (int argc, char** argv)
{
  ros::init (argc, argv, "keti_db_extractor");
  ros::NodeHandle priv_nh("~");

  std::string source_location, result_location;
  std::string lidar_type;
  std::string illuminance_topic, imu_topic, gps_topic;
  std::string ouster_metadata;

  priv_nh.param<std::string>("source_location", source_location, "/home/moon/extraDB/ROSBAG/seg_lidar_2021-04-02-07-13-02.bag");
  priv_nh.param<std::string>("result_location", result_location, "/home/moon/test");

  std::vector<std::string> bagfile_paths, result_locations, bagfile_names;

  if(source_location.substr(source_location.find_last_of(".") + 1) == "bag")
  {
    bagfile_paths.push_back(source_location);
    bagfile_paths.emplace_back(source_location);
    std::istringstream ss(source_location);
    std::string file_name;
    while(getline(ss, file_name, '/')){}
    std::string bagfile_name = "/" + file_name.substr(0, file_name.size() - 4);
    bagfile_names.push_back(bagfile_name);
    result_location += "/" + file_name.substr(0, file_name.size() - 4);
    result_locations.emplace_back(result_location);
  }
  else
  {
    for (const auto & entry : fsys::directory_iterator(source_location)){
      std::string path = entry.path().c_str();
      if(path.substr(path.find_last_of(".") + 1) == "bag"){
        bagfile_paths.emplace_back(entry.path().c_str());
        std::istringstream ss(entry.path().c_str());
        std::string file_name;
        while(getline(ss, file_name, '/')){}
        std::string bagfile_name = "/" + file_name.substr(0, file_name.size() - 4);
        bagfile_names.push_back(bagfile_name);
        std::string ret_loc = result_location + "/" + file_name.substr(0, file_name.size() - 4);
        result_locations.emplace_back(ret_loc);
      }
    }
  }

  for(int bag_idx = 0; bag_idx < bagfile_names.size(); bag_idx++){
    std::vector<std::string> point_cloud_topic_list;
    priv_nh.getParam("point_cloud_topic_list", point_cloud_topic_list);
    priv_nh.param<std::string>("lidar_type", lidar_type, "velodyne");
    std::cout << "[LiDAR type]" << lidar_type << std::endl;
    std::cout << "[Point cloud topic]" << std::endl;
    for (int i = 0; i < point_cloud_topic_list.size(); i ++){
      std::cout << point_cloud_topic_list[i] << std::endl;
    }

    std::vector<std::string> camera_image_topic_list;
    priv_nh.getParam("camera_image_topic_list", camera_image_topic_list);
    std::cout << "[Camera image topic]" << std::endl;
    for (int i = 0; i < camera_image_topic_list.size(); i ++){
      std::cout << camera_image_topic_list[i] << std::endl;
    }

    std::vector<std::string> radar_topic_list;
    priv_nh.getParam("radar_topic_list", radar_topic_list);
    std::cout << "[Radar topic]" << std::endl;
    for (int i = 0; i < radar_topic_list.size(); i ++){
      std::cout << radar_topic_list[i] << std::endl;
    }

    priv_nh.param<std::string>("imu_topic", imu_topic, "/imu");
    std::cout << "[imu] : " << imu_topic.c_str() << std::endl;
    priv_nh.param<std::string>("gps_topic", gps_topic, "/inspva");
    std::cout << "[gps] : " << gps_topic.c_str() << std::endl;

    priv_nh.param<std::string>("illuminance_topic", illuminance_topic, "/als20/illuminance");
    std::cout << "[illuminance] : " << illuminance_topic.c_str() << std::endl;

    rosbag::Bag bag;
    rosbag::View view;
    rosbag::View::iterator view_it;

    priv_nh.param<std::string>("ouster_metadata", ouster_metadata, "xxx.json");
    int n_returns = 0;
    ouster::XYZLut xyz_lut;
    ouster::LidarScan ls;
    ouster_ros::Cloud ouster_cloud;
    std::unique_ptr<ouster::ScanBatcher> scan_batcher;
    if (!ouster_metadata.empty())
    {
      ouster_sensor::sensor_info ouster_info;
      ouster_info = ouster_sensor::metadata_from_json(ouster_metadata);

      uint32_t H = ouster_info.format.pixels_per_column;
      uint32_t W = ouster_info.format.columns_per_frame;
      n_returns = ouster_info.format.udp_profile_lidar ==
          ouster_sensor::UDPProfileLidar::PROFILE_RNG19_RFL8_SIG16_NIR16_DUAL
          ? 2
          : 1;
      xyz_lut = ouster::make_xyz_lut(ouster_info);
      ls = ouster::LidarScan{W, H, ouster_info.format.udp_profile_lidar};
      ouster_cloud = ouster_ros::Cloud{W, H};
      scan_batcher = std::make_unique<ouster::ScanBatcher>(ouster_info);

      std::cout << "[Ouster metadata]" << std::endl;
      std::cout << "Using lidar_mode: " << ouster_sensor::to_string(ouster_info.mode).c_str() << std::endl;
      std::cout << ouster_info.prod_line.c_str() << "\n"
                << "SN : " << ouster_info.sn.c_str() << "\n"
                << "firmware rev : " << ouster_info.fw_rev.c_str() << std::endl;
    }

    try
    {
      std::cout << "Read " << bagfile_paths[bag_idx] << std::endl;
      bag.open (bagfile_paths[bag_idx], rosbag::bagmode::Read);
    }
    catch (const rosbag::BagException&)
    {
      std::cerr << "Error opening file " << bagfile_paths[bag_idx] << std::endl;
      return (-1);
    }

    // check that target topic exists in the bag file:
    rosbag::View topic_list_view(bag);
    std::string target_topic, target_topic2;
    for(rosbag::ConnectionInfo const *ci: topic_list_view.getConnections() )
    {
      if (std::find(point_cloud_topic_list.begin(), point_cloud_topic_list.end(), ci->topic) != point_cloud_topic_list.end()) // for point cloud topic
      {
        std::vector<std::string>::iterator it = std::find(point_cloud_topic_list.begin(), point_cloud_topic_list.end(), ci->topic);
        if (ci->datatype == std::string("sensor_msgs/PointCloud2"))
          view.addQuery (bag, rosbag::TopicQuery(*it));
        else if(ci->datatype == std::string("ouster_ros/PacketMsg"))
          view.addQuery (bag, rosbag::TopicQuery(*it));
      }
      else if(std::find(camera_image_topic_list.begin(), camera_image_topic_list.end(), ci->topic) != camera_image_topic_list.end()) // for camera topic
      {
        std::vector<std::string>::iterator it = std::find(camera_image_topic_list.begin(), camera_image_topic_list.end(), ci->topic);
        if(ci->datatype == "sensor_msgs/Image")
          view.addQuery(bag, rosbag::TopicQuery(*it));
        else if(ci->datatype == "sensor_msgs/CompressedImage")
          view.addQuery(bag, rosbag::TopicQuery(*it));
      }
      else if(std::find(radar_topic_list.begin(), radar_topic_list.end(), ci->topic) != radar_topic_list.end()) // for radar topic
      {
        std::vector<std::string>::iterator it = std::find(radar_topic_list.begin(), radar_topic_list.end(), ci->topic);
        if(ci->datatype == "control_msgs/ChassisRadarData")
          view.addQuery(bag, rosbag::TopicQuery(*it));
      }
      else if(ci->topic == illuminance_topic)
      {
        if(ci->datatype == std::string("als20_ambient_light_sensor/Illuminance"))
        {
          view.addQuery(bag, rosbag::TopicQuery(illuminance_topic));
        }
      }
      else if(ci->topic == imu_topic)
      {
        if(ci->datatype == std::string("sensor_msgs/Imu"))
          view.addQuery(bag, rosbag::TopicQuery(imu_topic));
      }
      else if(ci->topic == gps_topic)
      {
        if(ci->datatype == std::string("novatel_gps_msgs/Inspva"))
          view.addQuery(bag, rosbag::TopicQuery(gps_topic));
        else if(ci->datatype == std::string("novatel_oem7_msgs/INSPVA"))
          view.addQuery(bag, rosbag::TopicQuery(gps_topic));
      }
    }

    boost::filesystem::path outpath (result_locations[bag_idx]);
    if (!boost::filesystem::exists(outpath))
    {
      if (!boost::filesystem::create_directories(outpath))
      {
        std::cerr << "Error creating directory " << result_locations[bag_idx] << std::endl;
        return (-1);
      }
      std::cerr << "Creating directory " << result_locations[bag_idx] << std::endl;
    }

    for (int i = 0; i < point_cloud_topic_list.size(); i++){
      outpath = result_locations[bag_idx] + "/LiDAR/LiDAR" + std::to_string(i);
      if (!boost::filesystem::exists(outpath))
      {
        if (!boost::filesystem::create_directories(outpath))
        {
          std::cerr << "Error creating directory " << outpath << std::endl;
          return (-1);
        }
        std::cerr << "Creating directory " << outpath << std::endl;
      }
    }

    for (int i = 0; i < camera_image_topic_list.size(); i++){
      outpath = result_locations[bag_idx] + "/Camera/Camera" + std::to_string(i);
      if (!boost::filesystem::exists(outpath))
      {
        if (!boost::filesystem::create_directories(outpath))
        {
          std::cerr << "Error creating directory " << outpath << std::endl;
          return (-1);
        }
        std::cerr << "Creating directory " << outpath << std::endl;
      }
    }

    for (int i = 0; i < radar_topic_list.size(); i++){
      outpath = result_locations[bag_idx] + "/Radar/Radar" + std::to_string(i);
      if (!boost::filesystem::exists(outpath))
      {
        if (!boost::filesystem::create_directories(outpath))
        {
          std::cerr << "Error creating directory " << outpath << std::endl;
          return (-1);
        }
        std::cerr << "Creating directory " << outpath << std::endl;
      }
    }

    //    outpath = result_locations[bag_idx] + "/Vehicle_info";
    //    if (!boost::filesystem::exists(outpath))
    //    {
    //      if (!boost::filesystem::create_directories(outpath))
    //      {
    //        std::cerr << "Error creating directory " << result_locations[bag_idx] << "/Vehicle_info" << std::endl;
    //        return (-1);
    //      }
    //      std::cerr << "Creating directory " << result_locations[bag_idx] << "/Vehicle_info" << std::endl;
    //    }

    outpath = result_locations[bag_idx] + "/illuminance";
    if (!boost::filesystem::exists(outpath))
    {
      if (!boost::filesystem::create_directories(outpath))
      {
        std::cerr << "Error creating directory " << result_locations[bag_idx] << "/illuminance" << std::endl;
        return (-1);
      }
      std::cerr << "Creating directory " << result_locations[bag_idx] << "/illuminance" << std::endl;
    }

    outpath = result_locations[bag_idx] + "/IMU";
    if (!boost::filesystem::exists(outpath))
    {
      if (!boost::filesystem::create_directories(outpath))
      {
        std::cerr << "Error creating directory " << result_locations[bag_idx] << "/IMU" << std::endl;
        return (-1);
      }
      std::cerr << "Creating directory " << result_locations[bag_idx] << "/IMU" << std::endl;
    }

    outpath = result_locations[bag_idx] + "/GPS";
    if (!boost::filesystem::exists(outpath))
    {
      if (!boost::filesystem::create_directories(outpath))
      {
        std::cerr << "Error creating directory " << result_locations[bag_idx] << "/GPS" << std::endl;
        return (-1);
      }
      std::cerr << "Creating directory " << result_locations[bag_idx] << "/GPS" << std::endl;
    }

    outpath = result_locations[bag_idx] + "/Sync";
    if (!boost::filesystem::exists(outpath))
    {
      if (!boost::filesystem::create_directories(outpath))
      {
        std::cerr << "Error creating directory " << result_locations[bag_idx] << "/Sync" << std::endl;
        return (-1);
      }
      std::cerr << "Creating directory " << result_locations[bag_idx] << "/Sync" << std::endl;
    }

    std::vector<int> cam_count(camera_image_topic_list.size());
    std::vector<std::string> image_ext(camera_image_topic_list.size());
    std::vector<int> lidar_count(point_cloud_topic_list.size());
    std::vector<int> radar_count(radar_topic_list.size());
    std::vector<bool> first_ouster_packet(point_cloud_topic_list.size(), true);
    int vstate_cnt, sync_cnt, illuminance_cnt, imu_cnt, gps_cnt;
    vstate_cnt = imu_cnt = gps_cnt = sync_cnt = illuminance_cnt = 0;

    // TODO : refactoring
    std::vector<std::ofstream> lidar_fs;
    for (int i = 0; i < point_cloud_topic_list.size(); i++)
    {
      std::string timestamp_file = result_locations[bag_idx] + "/LiDAR/lidar"+ std::to_string(i) + "_timestamp.txt";
      std::ofstream fs;
      fs.open(timestamp_file);
      lidar_fs.push_back(std::move(fs));
    }
    std::vector<std::ofstream> camera_fs;
    for (int i = 0; i < camera_image_topic_list.size(); i++)
    {
      std::string timestamp_file = result_locations[bag_idx] + "/Camera/cam" + std::to_string(i) + "_timestamp.txt";
      std::ofstream fs;
      fs.open(timestamp_file);
      camera_fs.push_back(std::move(fs));
    }
    std::vector<std::ofstream> radar_fs;
    for (int i = 0; i < radar_topic_list.size(); i++)
    {
      std::string timestamp_file = result_locations[bag_idx] + "/Radar/radar" + std::to_string(i) + "_timestamp.txt";
      std::ofstream fs;
      fs.open(timestamp_file);
      radar_fs.push_back(std::move(fs));
    }

    outpath = result_locations[bag_idx] + "/GPS/gps_timestamp.txt";
    boost::filesystem::ofstream gps_fs(outpath);
    outpath = result_locations[bag_idx] + "/IMU/imu_timestamp.txt";
    boost::filesystem::ofstream imu_fs(outpath);
    outpath = result_locations[bag_idx] + "/illuminance/illuminance_timestamp.txt";
    boost::filesystem::ofstream illuminance_fs(outpath);
    //  outpath = result_locations[bag_idx] + "/Vehicle_info/vstate_timestamp.txt";
    //  boost::filesystem::ofstream vstate_fs(outpath);

    int print_text_cnt = 0;

    struct Image{
      bool is_raw = false;
      unsigned long timestamp = 0;
      std::vector<uint8_t> data;
      cv::Mat MatData;
    };
    Image image;

    view_it = view.begin();
    ros::Time pre_cloud_time(0);
    while (view_it != view.end () && ros::ok())
    {
      std::cout << "[" << ros::Time::now() << "] processing data extract... [" << print_text_cnt << "/" << view.size() << "]\r" << std::flush;

      PointCloudPtr cloud = view_it->instantiate<PointCloud> ();
      ouster_ros::PacketMsg::ConstPtr ouster_packet = view_it->instantiate<ouster_ros::PacketMsg> ();

      sensor_msgs::CompressedImage::Ptr comp_image = view_it->instantiate<sensor_msgs::CompressedImage> ();
      if (comp_image != NULL){
        image.is_raw = false;
        image.timestamp = comp_image->header.stamp.toNSec();
        if (view_it->getTopic().find("yuv") != std::string::npos)
          image.MatData = cv_bridge::toCvCopy(comp_image, sensor_msgs::image_encodings::YUV422)->image;
        else
          image.data = comp_image->data;
      }
      sensor_msgs::Image::Ptr raw_image = view_it->instantiate<sensor_msgs::Image> ();
      if (raw_image != NULL){
        image.is_raw = true;
        image.timestamp = raw_image->header.stamp.toNSec();
        image.MatData = cv_bridge::toCvShare(raw_image, sensor_msgs::image_encodings::MONO16)->image;
      }

      sensor_msgs::Imu::Ptr imu = view_it->instantiate<sensor_msgs::Imu> ();
      novatel_gps_msgs::Inspva::Ptr gps = view_it->instantiate<novatel_gps_msgs::Inspva> ();
      novatel_oem7_msgs::INSPVA::Ptr oem7_gps = view_it->instantiate<novatel_oem7_msgs::INSPVA> ();
      can_msgs::Frame::Ptr can = view_it->instantiate<can_msgs::Frame> ();
      control_msgs::ChassisRadarData::Ptr radar = view_it->instantiate<control_msgs::ChassisRadarData> ();
      als20_ambient_light_sensor::Illuminance::Ptr illuminance = view_it->instantiate<als20_ambient_light_sensor::Illuminance> ();
      //    Ptr vstate = view_it->instantiate<> ();

      if(image.timestamp == 0 && (cloud == NULL || ouster_packet == NULL) && (gps == NULL || oem7_gps) && imu == NULL/*&& vstate == NULL*/)
      {
        ++view_it;
        continue;
      }

      if(lidar_type == "ouster_packet" && ouster_packet != NULL)
      {
        auto packet_receive_time = view_it->getTime();
        static auto frame_ts = packet_receive_time;
        if ((*scan_batcher)(ouster_packet->buf.data(), ls))
        {
          auto ts_v = ls.timestamp();
          auto idx = std::find_if(ts_v.data(), ts_v.data() + ts_v.size(),
                                  [](uint64_t h) { return h != 0; });
          if (!(idx == ts_v.data() + ts_v.size()))
          {
            auto scan_ts = std::chrono::nanoseconds{ts_v(idx - ts_v.data())};
            for (int i = 0; i < n_returns; ++i) {
              ouster_ros::scan_to_cloud(xyz_lut, scan_ts, ls, ouster_cloud, i);
              sensor_msgs::PointCloud2 pc =
                  ouster_ros::cloud_to_cloud_msg(ouster_cloud, frame_ts, "os_sensor");
              cloud = boost::make_shared<sensor_msgs::PointCloud2>(pc);
              frame_ts = packet_receive_time;
            }
          }
        }
      }

      if(cloud != NULL)
      {
        PointCloud cloud_t;

        if(lidar_type == "ouster" || lidar_type == "ouster_packet")
        {
          int x_idx = pcl::getFieldIndex (*cloud, "x");
          int y_idx = pcl::getFieldIndex (*cloud, "y");
          int z_idx = pcl::getFieldIndex (*cloud, "z");
          int i_idx = pcl::getFieldIndex (*cloud, "intensity");
          int r_idx = pcl::getFieldIndex (*cloud, "ring");
          int t_idx = pcl::getFieldIndex (*cloud, "t");

          std::vector<sensor_msgs::PointField> new_fieids;

          new_fieids.emplace_back(cloud->fields[x_idx]);
          new_fieids.emplace_back(cloud->fields[y_idx]);
          new_fieids.emplace_back(cloud->fields[z_idx]);
          new_fieids.emplace_back(cloud->fields[i_idx]);
          new_fieids.emplace_back(cloud->fields[r_idx]);
          new_fieids.emplace_back(cloud->fields[t_idx]);

          cloud->fields = new_fieids;
        }

        cloud_t = *cloud;

        std::stringstream pointcloud_ss;

        for (int i = 0; i < point_cloud_topic_list.size(); i++)
        {
          if(view_it->getTopic() == point_cloud_topic_list[i])
          {
            int invalid_cnt = 0;
            bool invalid = false;
            for (size_t pt = 0; pt < cloud->width * cloud->height; pt++)
            {
              float x = sensor_msgs::readPointCloud2BufferValue<float>(&cloud->data[pt * cloud->point_step + cloud->fields[0].offset], cloud->fields[0].datatype);
              float y = sensor_msgs::readPointCloud2BufferValue<float>(&cloud->data[pt * cloud->point_step + cloud->fields[1].offset], cloud->fields[1].datatype);
              float z = sensor_msgs::readPointCloud2BufferValue<float>(&cloud->data[pt * cloud->point_step + cloud->fields[2].offset], cloud->fields[2].datatype);
              float intensity = sensor_msgs::readPointCloud2BufferValue<float>(&cloud->data[pt * cloud->point_step + cloud->fields[3].offset], cloud->fields[3].datatype);

              if (x == .0f && y == .0f && z == .0f && intensity == .0f) { invalid_cnt++; }
              if (invalid_cnt >= (int)(cloud->width * cloud->height * 0.09)) { invalid = true; break; }
            }

            if (!invalid)
            {
              lidar_fs[i] << cloud->header.stamp.toNSec() << std::endl;
              std::string file_path = "/LiDAR/LiDAR" + std::to_string(i) + "/lidar" + std::to_string(i) + "_";
              pointcloud_ss << result_locations[bag_idx] << file_path << std::setw(6) << std::setfill('0') << lidar_count[i] << ".pcd";
              pcl::io::savePCDFile(pointcloud_ss.str(), cloud_t, Eigen::Vector4f::Zero(), Eigen::Quaternionf::Identity(), false);
              lidar_count[i]++;
              break;
            }
          }
        }
      }

      if(radar != NULL)
      {
        for (int i = 0; i < radar_topic_list.size(); i++)
        {
          if(view_it->getTopic() == radar_topic_list[i])
          {
            radar_fs[i].precision(12);
            radar_fs[i] << radar->timestamp.toNSec() << std::endl;

            std::stringstream radar_txt_loc_ss;
            std::string file_path = "/Radar/Radar" + std::to_string(i) + "/radar" + std::to_string(i) + "_";
            radar_txt_loc_ss << result_locations[bag_idx] << file_path << std::setw(6) << std::setfill('0') << radar_count[i] << ".txt";

            boost::filesystem::ofstream radar_txt_ss(radar_txt_loc_ss.str());
            radar_txt_ss << "obj_status : " << (int)radar->obj_status << std::endl;
            radar_txt_ss << "obj_lat_pos : " << (float)radar->obj_lat_pos << std::endl;
            radar_txt_ss << "obj_lon_dist : " << (float)radar->obj_lon_dist << std::endl;
            radar_txt_ss << "obj_rel_vel : " << (float)radar->obj_rel_vel << std::endl;
            radar_count[i]++;
          }
        }
      }

      if(illuminance != NULL)
      {
        if(view_it->getTopic() == illuminance_topic)
        {
          illuminance_fs.precision(12);
          illuminance_fs << illuminance->header.stamp.toNSec() << std::endl;

          std::stringstream illuminance_txt_loc_ss;
          std::string file_path = "/illuminance/illuminance_";
          illuminance_txt_loc_ss << result_locations[bag_idx] << file_path << std::setw(6) << std::setfill('0') << illuminance_cnt << ".txt";

          boost::filesystem::ofstream illuminance_txt_ss(illuminance_txt_loc_ss.str());
          illuminance_txt_ss << "status : " << (int)illuminance->sensor_status << std::endl;
          illuminance_txt_ss << "illuminance : " << (int)illuminance->illuminance << std::endl;
          illuminance_cnt++;
        }
      }

      if(gps != NULL)
      {
        gps_fs.precision(12);
        gps_fs << gps->header.stamp.toNSec() << " " << gps->latitude << " " <<
                  gps->longitude << " " << gps->height << " " <<
                  gps->roll << " " << gps->pitch << " " << gps->azimuth << std::endl;
        gps_cnt++;
      }
      else if(oem7_gps != NULL)
      {
        gps_fs.precision(12);
        gps_fs << oem7_gps->header.stamp.toNSec() << " " << oem7_gps->latitude << " " <<
                  oem7_gps->longitude << " " << oem7_gps->height << " " <<
                  oem7_gps->roll << " " << oem7_gps->pitch << " " << oem7_gps->azimuth << std::endl;
        gps_cnt++;
      }

      if(imu != NULL)
      {
        imu_fs.precision(12);
        imu_fs << imu->header.stamp.toNSec() << " " <<
                  imu->linear_acceleration.x << " " << imu->linear_acceleration.x << " " << imu->linear_acceleration.x << " " <<
                  imu->angular_velocity.x << " " << imu->angular_velocity.y << " " << imu->angular_velocity.z << std::endl;
        imu_cnt++;
      }

      /*
    if(vstate != NULL)
    {
      vstate_fs.precision(12);
      vstate_fs << vstate->header.stamp.toNSec() << std::endl;
      vstate_cnt++;
    }
    */

      if(image.timestamp != 0)
      {
        std::stringstream image_ss;

        for (int i = 0; i < camera_image_topic_list.size(); i++){
          if(view_it->getTopic() == camera_image_topic_list[i])
          {
            camera_fs[i] << image.timestamp << std::endl;
            std::string file_path = "/Camera/Camera" + std::to_string(i) + "/cam" + std::to_string(i) + "_";
            image_ss << result_locations[bag_idx] << file_path << std::setw(6) << std::setfill('0') << cam_count[i];
            cv::Mat final_image;
            if (image.is_raw){
              final_image = image.MatData;
              image_ss << ".png";
              image_ext[i] = ".png";
            }
            else
            {
              if (view_it->getTopic().find("yuv") != std::string::npos)
                final_image = image.MatData;
              else
                final_image = cv::imdecode(cv::Mat(image.data), 1); // for sensor_image/compressedImage
              image_ss << ".jpg";
              image_ext[i] = ".jpg";
            }
            cv::imwrite(image_ss.str(), final_image);
            cam_count[i]++;
            break;
          }
        }
      }

      print_text_cnt++;
      ++view_it;
    }

    std::cout << "\nstart sync process" << std::endl;

    int pivot_cam_cnt;
    pivot_cam_cnt = imu_cnt = gps_cnt = illuminance_cnt = 0;

    std::vector<std::ifstream> lidar_ifs;
    for (int i = 0; i < point_cloud_topic_list.size(); i++){
      std::string timestamp_file = result_locations[bag_idx] + "/LiDAR/lidar" + std::to_string(i) + "_timestamp.txt";
      std::ifstream ifs;
      ifs.open(timestamp_file);
      lidar_ifs.push_back(std::move(ifs));
    }

    std::vector<std::ifstream> cam_ifs;
    for (int i = 0; i < camera_image_topic_list.size(); i++){
      std::string timestamp_file = result_locations[bag_idx] + "/Camera/cam" + std::to_string(i) + "_timestamp.txt";
      std::ifstream ifs;
      ifs.open(timestamp_file);
      cam_ifs.push_back(std::move(ifs));
    }

    std::vector<std::ifstream> radar_ifs;
    for (int i = 0; i < radar_topic_list.size(); i++)
    {
      std::string timestamp_file = result_locations[bag_idx] + "/Radar/radar" + std::to_string(i) + "_timestamp.txt";
      std::ifstream fs;
      fs.open(timestamp_file);
      radar_ifs.push_back(std::move(fs));
    }

    outpath = result_locations[bag_idx] + "/IMU/imu_timestamp.txt";
    boost::filesystem::ifstream imu_ifs(outpath);
    outpath = result_locations[bag_idx] + "/GPS/gps_timestamp.txt";
    boost::filesystem::ifstream gps_ifs(outpath);

    outpath = result_locations[bag_idx] + "/illuminance/illuminance_timestamp.txt";
    boost::filesystem::ifstream illuminance_ifs(outpath);
    //  outpath = result_locations[bag_idx] + "/Vehicle_info/vstate_timestamp.txt";
    //  boost::filesystem::ifstream vstate_ifs(outpath);

    long pre_pivot_cam_timestamp = 0;
    print_text_cnt = 0;

    bool imu_flag, gps_flag;
    imu_flag = gps_flag = false;

    if(cam_ifs[0].is_open())
    {
      std::vector<int> final_cam_cnt(camera_image_topic_list.size());
      std::vector<int> cam_cnt(camera_image_topic_list.size());
      std::vector<long> pre_cam_timestamp(camera_image_topic_list.size());

      std::vector<int> final_radar_cnt(radar_topic_list.size());
      std::vector<int> radar_cnt(radar_topic_list.size());
      std::vector<long> pre_radar_timestamp(radar_topic_list.size());

      std::vector<int> final_lidar_cnt(point_cloud_topic_list.size());
      std::vector<int> lidar_cnt(point_cloud_topic_list.size());
      std::vector<long> pre_lidar_timestamp(point_cloud_topic_list.size(), 0);
      std::vector<long> after_lidar_timestamp(point_cloud_topic_list.size(), 0);

      std::vector<long> illuminance_timestamp;
      while(!illuminance_ifs.eof())
      {
        std::string illuminance_data_s;
        getline(illuminance_ifs, illuminance_data_s);
        if(illuminance_data_s != "")
          illuminance_timestamp.push_back(std::stol(illuminance_data_s));
      }
      int final_illuminance_cnt = 0;

      while(!cam_ifs[0].eof())
      {
        std::cout << "[" << ros::Time::now() << "] processing synchronize... [" << pivot_cam_cnt << "/" << cam_count[0] << "]\r" << std::flush;
        std::string pivot_cam_timestamp_s;
        getline(cam_ifs[0], pivot_cam_timestamp_s);
        long pivot_cam_timestamp;
        if(pivot_cam_timestamp_s != "")
          pivot_cam_timestamp = std::stol(pivot_cam_timestamp_s);
        else
          pivot_cam_timestamp = std::numeric_limits<long>::max();

        for (int i = 1; i < camera_image_topic_list.size(); i++){
          if(cam_ifs[i].is_open() && pivot_cam_timestamp != std::numeric_limits<long>::max())
          {
            while(!cam_ifs[i].eof())
            {
              std::string cam_data_s;
              getline(cam_ifs[i], cam_data_s);

              if(cam_data_s != "")
              {
                long cam_timestamp = std::stol(cam_data_s);

                if(cam_timestamp < pivot_cam_timestamp)
                {
                  pre_cam_timestamp[i] = cam_timestamp;
                  cam_cnt[i]++;
                }
                else
                {
                  if(pivot_cam_timestamp - pre_cam_timestamp[i] < cam_timestamp - pivot_cam_timestamp)
                    final_cam_cnt[i] = (cam_cnt[i] - 1);
                  else
                    final_cam_cnt[i] = cam_cnt[i];
                  cam_cnt[i]++;
                  break;
                }
              }
            }
          }
        }

        for (int i = 0; i < radar_topic_list.size(); i++){
          if(radar_ifs[i].is_open() && pivot_cam_timestamp != std::numeric_limits<long>::max())
          {
            while(!radar_ifs[i].eof())
            {
              std::string radar_data_s;
              getline(radar_ifs[i], radar_data_s);

              if(radar_data_s != "")
              {
                long radar_timestamp = std::stol(radar_data_s);

                if(radar_timestamp < pivot_cam_timestamp)
                {
                  pre_radar_timestamp[i] = radar_timestamp;
                  cam_cnt[i]++;
                }
                else
                {
                  if(pivot_cam_timestamp - pre_radar_timestamp[i] < radar_timestamp - pivot_cam_timestamp)
                    final_radar_cnt[i] = (radar_cnt[i] - 1);
                  else
                    final_radar_cnt[i] = radar_cnt[i];
                  radar_cnt[i]++;
                  break;
                }
              }
            }
          }
        }

        for (int i = 0; i < point_cloud_topic_list.size(); i++)
        {
          if(lidar_ifs[i].is_open() && pivot_cam_timestamp != std::numeric_limits<long>::max())
          {
            long lidar_timestamp;

            if(pre_lidar_timestamp[i] == 0 && after_lidar_timestamp[i] == 0)
            {
              std::string lidar_data_s;
              getline(lidar_ifs[i], lidar_data_s);
              if(lidar_data_s != "")
              {
                lidar_timestamp = std::stol(lidar_data_s);
                pre_lidar_timestamp[i] = lidar_timestamp;
              }

              getline(lidar_ifs[i], lidar_data_s);
              if(lidar_data_s != "")
              {
                lidar_timestamp = std::stol(lidar_data_s);
                after_lidar_timestamp[i] = lidar_timestamp;
              }
            }

            if(pivot_cam_timestamp < pre_lidar_timestamp[i])
            {
              final_lidar_cnt[i] = lidar_cnt[i];
              break;
            }

            if(pivot_cam_timestamp > after_lidar_timestamp[i])
            {
              if(!lidar_ifs[i].eof())
              {
                pre_lidar_timestamp[i] = after_lidar_timestamp[i];
                std::string lidar_data_s;
                getline(lidar_ifs[i], lidar_data_s);
                if(lidar_data_s != "")
                {
                  lidar_timestamp = std::stol(lidar_data_s);
                  after_lidar_timestamp[i] = lidar_timestamp;
                }

                lidar_cnt[i]++;
                final_lidar_cnt[i] = lidar_cnt[i];
                break;
              }
              else
              {
                final_lidar_cnt[i] = lidar_cnt[i];
                break;
              }
            }

            if(pivot_cam_timestamp - pre_lidar_timestamp[i] < after_lidar_timestamp[i] - pivot_cam_timestamp)
            {
              final_lidar_cnt[i] = lidar_cnt[i];
              break;
            }
            else
            {
              final_lidar_cnt[i] = lidar_cnt[i] + 1;
              break;
            }
          }

          //            if(lidar_data_s != "")
          //            {
          //              long lidar_timestamp = std::stol(lidar_data_s);

          //              if(lidar_timestamp < pivot_cam_timestamp)
          //              {
          //                pre_lidar_timestamp[i] = lidar_timestamp;
          //                lidar_cnt[i]++;
          //              }
          //              else
          //              {
          //                if(pivot_cam_timestamp - pre_lidar_timestamp[i] < lidar_timestamp - pivot_cam_timestamp)
          //                  final_lidar_cnt[i] = (lidar_cnt[i] - 1);
          //                else
          //                  final_lidar_cnt[i] = lidar_cnt[i];
          //                lidar_cnt[i]++;
          //                break;
          //              }
          //            }

          if(imu_ifs.is_open())
          {
            if(pre_pivot_cam_timestamp != 0)
            {
              std::stringstream imu_ss;
              imu_ss << std::setw(6) << std::setfill('0') << imu_cnt << ".txt";
              boost::filesystem::ofstream imu_if(result_locations[bag_idx] + "/IMU/imu_" + imu_ss.str());
              std::string pre_imu_data_s;
              long pre_read_pos, pre_pre_read_pos, pre_write_pos;

              while(!imu_ifs.eof())
              {
                pre_read_pos = imu_ifs.tellg();
                std::string imu_data_s, imu_timestamp_s;
                long pre_imu_timestamp;
                getline(imu_ifs, imu_data_s);

                if(imu_data_s != "")
                {
                  imu_flag = true;
                  std::istringstream imu_data(imu_data_s);
                  getline(imu_data, imu_timestamp_s, ' ');
                  long imu_timestamp = std::stol(imu_timestamp_s);

                  if(pre_pivot_cam_timestamp > imu_timestamp)
                  {
                    pre_imu_data_s = imu_data_s;
                    pre_imu_timestamp = imu_timestamp;
                  }
                  else if(imu_timestamp > pre_pivot_cam_timestamp && pivot_cam_timestamp > imu_timestamp)
                  {
                    if(pre_pivot_cam_timestamp > pre_imu_timestamp &&
                       pre_pivot_cam_timestamp - pre_imu_timestamp < imu_timestamp - pre_pivot_cam_timestamp)
                    {
                      imu_if << pre_imu_data_s << std::endl;
                    }

                    pre_write_pos = imu_if.tellp();
                    imu_if << imu_data_s << std::endl;
                    pre_imu_timestamp = imu_timestamp;
                  }
                  else if(imu_timestamp > pivot_cam_timestamp)
                  {
                    if(pivot_cam_timestamp - pre_imu_timestamp < imu_timestamp - pivot_cam_timestamp)
                    {
                      boost::filesystem::ifstream origin_imu(result_locations[bag_idx] + "/IMU/imu_" + imu_ss.str());
                      boost::filesystem::ofstream temp_imu(result_locations[bag_idx] + "/IMU/temp.txt");
                      while(origin_imu.tellg() != pre_write_pos)
                      {
                        std::string line;
                        getline(origin_imu, line);
                        temp_imu << line << std::endl;
                      }
                      boost::filesystem::remove(result_locations[bag_idx] + "/IMU/imu_" + imu_ss.str());
                      boost::filesystem::rename(result_locations[bag_idx] + "/IMU/temp.txt", result_locations[bag_idx] + "/IMU/imu_" + imu_ss.str());
                      imu_ifs.seekg(pre_pre_read_pos);
                    }
                    else{
                      imu_ifs.seekg(pre_read_pos);
                    }
                    break;
                  }
                }
                pre_pre_read_pos = pre_read_pos;
              }

              if (imu_flag)
                imu_cnt++;
            }
          }

          if(gps_ifs.is_open())
          {
            if(pre_pivot_cam_timestamp != 0)
            {
              std::stringstream gps_ss;
              gps_ss << std::setw(6) << std::setfill('0') << gps_cnt << ".txt";
              boost::filesystem::ofstream gps_if(result_locations[bag_idx] + "/GPS/gps_" + gps_ss.str());
              std::string pre_gps_data_s;
              long pre_read_pos, pre_pre_read_pos, pre_write_pos;

              while(!gps_ifs.eof())
              {
                gps_flag = true;
                pre_read_pos = gps_ifs.tellg();
                std::string gps_data_s, gps_timestamp_s;
                long pre_gps_timestamp;
                getline(gps_ifs, gps_data_s);

                if(gps_data_s != "")
                {
                  std::istringstream gps_data(gps_data_s);
                  getline(gps_data, gps_timestamp_s, ' ');
                  long gps_timestamp = std::stol(gps_timestamp_s);

                  if(pre_pivot_cam_timestamp > gps_timestamp)
                  {
                    pre_gps_data_s = gps_data_s;
                    pre_gps_timestamp = gps_timestamp;
                  }
                  else if(gps_timestamp > pre_pivot_cam_timestamp && pivot_cam_timestamp > gps_timestamp)
                  {
                    if(pre_pivot_cam_timestamp > pre_gps_timestamp &&
                       pre_pivot_cam_timestamp - pre_gps_timestamp < gps_timestamp - pre_pivot_cam_timestamp)
                    {
                      gps_if << pre_gps_data_s << std::endl;
                    }

                    pre_write_pos = gps_if.tellp();
                    gps_if << gps_data_s << std::endl;
                    pre_gps_timestamp = gps_timestamp;
                  }
                  else if(gps_timestamp > pivot_cam_timestamp)
                  {
                    if(pivot_cam_timestamp - pre_gps_timestamp < gps_timestamp - pivot_cam_timestamp)
                    {
                      boost::filesystem::ifstream origin_gps(result_locations[bag_idx] + "/GPS/gps_" + gps_ss.str());
                      boost::filesystem::ofstream temp_gps(result_locations[bag_idx] + "/GPS/temp.txt");
                      while(origin_gps.tellg() != pre_write_pos)
                      {
                        std::string line;
                        getline(origin_gps, line);
                        temp_gps << line << std::endl;
                      }
                      boost::filesystem::remove(result_locations[bag_idx] + "/GPS/gps_" + gps_ss.str());
                      boost::filesystem::rename(result_locations[bag_idx] + "/GPS/temp.txt", result_locations[bag_idx] + "/GPS/gps_" + gps_ss.str());
                      gps_ifs.seekg(pre_pre_read_pos);
                    }
                    else
                      gps_ifs.seekg(pre_read_pos);
                    break;
                  }
                }
                pre_pre_read_pos = pre_read_pos;
              }

              if (gps_flag)
                gps_cnt++;
            }
          }
        }

        long min_illumianace_ts = std::numeric_limits<long>::max();
        for(int i = final_illuminance_cnt; i < illuminance_timestamp.size(); i++)
        {
          if(min_illumianace_ts > std::abs(illuminance_timestamp[i] - pivot_cam_timestamp))
          {
            min_illumianace_ts = std::abs(illuminance_timestamp[i] - pivot_cam_timestamp);
            final_illuminance_cnt = i;
          }
        }

        /*
      if(vstate_ifs.is_open())
      {
        if(pre_pivot_cam_timestamp != 0)
        {
          std::stringstream vstate_ss;
          vstate_ss << std::setw(6) << std::setfill('0') << vstate_cnt << ".txt";
          boost::filesystem::ofstream vstate_if(result_locations[bag_idx] + "/vstate/vstate_" + vstate_ss.str());
          std::string pre_vstate_data_s;
          long pre_read_pos, pre_pre_read_pos, pre_write_pos;

          while(!vstate_ifs.eof())
          {
            pre_read_pos = vstate_ifs.tellg();
            std::string vstate_data_s, vstate_timestamp_s;
            long pre_vstate_timestamp;
            getline(vstate_ifs, vstate_data_s);

            if(vstate_data_s != "")
            {
              std::istringstream vstate_data(vstate_data_s);
              getline(vstate_data, vstate_timestamp_s, ' ');
              long vstate_timestamp = std::stol(vstate_timestamp_s);

              if(pre_pivot_cam_timestamp > vstate_timestamp)
              {
                pre_vstate_data_s = vstate_data_s;
                pre_vstate_timestamp = vstate_timestamp;
              }
              else if(vstate_timestamp > pre_pivot_cam_timestamp && pivot_cam_timestamp > vstate_timestamp)
              {
                if(pre_pivot_cam_timestamp > pre_vstate_timestamp &&
                   pre_pivot_cam_timestamp - pre_vstate_timestamp < vstate_timestamp - pre_pivot_cam_timestamp)
                {
                  vstate_if << pre_vstate_data_s << std::endl;
                }

                pre_write_pos = vstate_if.tellp();
                vstate_if << vstate_data_s << std::endl;
                pre_vstate_timestamp = vstate_timestamp;
              }
              else if(vstate_timestamp > pivot_cam_timestamp)
              {
                if(pivot_cam_timestamp - pre_vstate_timestamp < vstate_timestamp - pivot_cam_timestamp)
                {
                  boost::filesystem::ifstream origin_vstate(result_locations[bag_idx] + "/vstate/vstate_" + vstate_ss.str());
                  boost::filesystem::ofstream temp_vstate(result_locations[bag_idx] + "/vstate/temp.txt");
                  while(origin_vstate.tellg() != pre_write_pos)
                  {
                    std::string line;
                    getline(origin_vstate, line);
                    temp_vstate << line << std::endl;
                  }
                  boost::filesystem::remove(result_locations[bag_idx] + "/vstate/vstate_" + vstate_ss.str());
                  boost::filesystem::rename(result_locations[bag_idx] + "/vstate/temp.txt", result_locations[bag_idx] + "/vstate/vstate_" + vstate_ss.str());
                  vstate_ifs.seekg(pre_pre_read_pos);
                }
                else
                  vstate_ifs.seekg(pre_read_pos);
                break;
              }
            }
            pre_pre_read_pos = pre_read_pos;
          }
          vstate_cnt++;
        }
      }
      */
        bool cam_ifs_eof = false;

        for (int i = 0; i < camera_image_topic_list.size(); i++)
        {
          if (cam_ifs[i].eof()){
            cam_ifs_eof = true;
            break;
          }
        }

        if(!cam_ifs_eof/*&& !vstate_ifs.eof()*/)
        {
          std::stringstream sync_ss;
          sync_ss << std::setw(6) << std::setfill('0') << sync_cnt << ".txt";
          boost::filesystem::ofstream sync_fs(result_locations[bag_idx] + "/Sync/sync_" + sync_ss.str());
          sync_fs << bagfile_names[bag_idx] << "/Camera/Camera0/cam0_" << std::setw(6) << std::setfill('0') << pivot_cam_cnt << image_ext[0] << std::endl;
          for (int i = 1; i < camera_image_topic_list.size(); i ++){
            sync_fs << bagfile_names[bag_idx] << "/Camera/Camera" + std::to_string(i) + "/cam" + std::to_string(i) + "_" <<
                       std::setw(6) << std::setfill('0') << final_cam_cnt[i] << image_ext[i] << std::endl;
          }
          for (int i = 0; i < radar_topic_list.size(); i ++){
            sync_fs << bagfile_names[bag_idx] << "/Radar/Radar" + std::to_string(i) + "/radar" + std::to_string(i) + "_" <<
                       std::setw(6) << std::setfill('0') << final_radar_cnt[i] << ".txt" << std::endl;
          }
          for (int i = 0; i < point_cloud_topic_list.size(); i ++){
            sync_fs << bagfile_names[bag_idx] << "/LiDAR/LiDAR" + std::to_string(i) + "/lidar" + std::to_string(i) + "_" <<
                       std::setw(6) << std::setfill('0') << final_lidar_cnt[i] << ".pcd" << std::endl;
          }
          if(imu_flag && gps_flag)
          {
            sync_fs << bagfile_names[bag_idx] << "/GPS/gps_" << std::setw(6) << std::setfill('0') << gps_cnt << ".txt" << std::endl;
            sync_fs << bagfile_names[bag_idx] << "/IMU/imu_" << std::setw(6) << std::setfill('0') << imu_cnt << ".txt" << std::endl;
          }

          sync_fs << bagfile_names[bag_idx] << "/illuminance/illuminance_" << std::setw(6) << std::setfill('0') << final_illuminance_cnt << ".txt" << std::endl;
          // sync_fs << bagfile_names[bag_idx] << "/Vehicle_info/vehicle_info_" << std::setw(6) << std::setfill('0') << vstate_cnt << ".txt" << std::endl;

          pre_pivot_cam_timestamp = pivot_cam_timestamp;
          sync_cnt++;
          pivot_cam_cnt++;
        }
        print_text_cnt++;
      }
    }
    else
      std::cout << "\nerror" << std::endl;

    for (int i = 0; i < point_cloud_topic_list.size(); i ++){
      boost::filesystem::remove(result_locations[bag_idx] + "/LiDAR/lidar" + std::to_string(i) + "_timestamp.txt");
    }
    for (int i = 0; i < camera_image_topic_list.size(); i ++){
      boost::filesystem::remove(result_locations[bag_idx] + "/Camera/cam" + std::to_string(i) + "_timestamp.txt");
    }
    for (int i = 0; i < radar_topic_list.size(); i ++){
      boost::filesystem::remove(result_locations[bag_idx] + "/Radar/radar" + std::to_string(i) + "_timestamp.txt");
    }
    boost::filesystem::remove(result_locations[bag_idx] + "/GPS/gps_timestamp.txt");
    boost::filesystem::remove(result_locations[bag_idx] + "/IMU/imu_timestamp.txt");
    boost::filesystem::remove(result_locations[bag_idx] + "/Vehicle_info/vstate_timestamp.txt");

    std::cout << "\nFinished all process." << std::endl;
  }

  return 0;
}
